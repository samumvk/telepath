<?php
$projectRoot = __DIR__;
require_once $projectRoot . '/libs/smarty/Smarty.class.php';
$smarty = new Smarty;
$smarty->template_dir = $projectRoot . '/tpls/templates';
$smarty->compile_dir = $projectRoot . '/tpls/templates_c';

$smarty->cache_dir = $projectRoot . '/tpls/cache';
$smarty->compile_check = true;
$smarty->debugging = false;