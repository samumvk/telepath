<?php


class Telepath
{

    function Telepath($name)
    {
        $this->name = "Telepath " . $name;
    }

    var $name; //��� ��������
    var $rate = 0; // �������
    var $prediction; //������� �������������
    var $history = array(); //������� ������������


    /**
     * ������ ��������
     * @param $realNumber ���������� �����
     */
    function calcRate($realNumber)
    {
        if ($this->prediction == $realNumber)
            $this->rate++;
        else
            $this->rate--;
    }

    /**
     * �������� ������������
     */
    function createPrediction()
    {
        $this->prediction = rand(10, 99);
        $this->history[] = $this->prediction;
    }

    /**
     * ��������� ����� ��� ������������
     * ������� ������ ���������� � ���� ������
     */
    function toString()
    {
        echo
            "Name: " . $this->name . " " .
            "Rate: " . $this->rate . " " .
            "Predication: " . $this->prediction . " ";
        echo "History: (";
        for ($i = 0; $i < count($this->history); $i++) {
            echo " | " . $this->history[$i];
        }
        echo ")";
    }
}