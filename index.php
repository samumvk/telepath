<?php
include_once "conf.php";
include_once "controller/TelepathController.php";

session_start();

$controller = new TelepathController();

$number = $_GET["number"];
$action = $_GET["action"];

if($action == 'newGame')
    $controller->newGame();
//������� ���������
if ($action == 'new')
    $controller->createTelepaths();

if ($controller->hasTelepaths()){
    //��������� �������
    $setRealNumber=$controller->setRealNumber($number);
    //������ ��� ����� �����
    if(!$setRealNumber&&$action == 'answer'){header('Location: index.php?action=new');}
    //�������� ������������
    if ($action != 'answer')$controller->makeNewPredictions();
}


//MVC ��������� html
$smarty->assign("number", $number);
$smarty->assign("action", $action);
$smarty->assign("numberHistory", $controller->getNumberHistory());
$smarty->assign("telepaths", $controller->getTelepaths());

$smarty->display("main.html");
