<?php
include_once "classes/Telepath.php";


class TelepathController
{
    var $realNumbers = array();
    function TelepathController()
    {
        if (isset($_SESSION["telepath"]))
            $this->telepaths = $_SESSION["telepath"];

        if (isset($_SESSION["realNumbers"]))
            $this->realNumbers = $_SESSION["realNumbers"];
    }
    var $telepathCount = 5;
    var $telepaths = array();
    /**
     * �������� ���������
     */
    function createTelepaths()
    {
        if (!isset($_SESSION["telepath"])){
        $this->telepaths = array();
        for ($i = 0; $i < $this->telepathCount; $i++) {
            $this->telepaths[] = new Telepath($i);
        }
        $_SESSION["telepath"] = $this->telepaths;
        }

    }

    /**
     * ����� ����
     */
    function newGame(){
        session_destroy();
        header('Location: index.php');
    }
    /**
     * @return bool ���� �������� ������� �� true
     */
    function hasTelepaths(){
        return (count($this->telepaths) > 0);
    }
    /**
     * �������� ���������
     */
    function makeNewPredictions()
    {
        if (count($this->telepaths) == 0)
            return false;

        for ($i = 0; $i < $this->telepathCount; $i++) {
            $telepath = $this->telepaths[$i];
            $telepath->createPrediction();
        }
    }
    /**
     * ������ ���������� ����� ���������
     * @param $number ���������� �����
     */
    function setRealNumber($number)
    {
        if ($number == null || $number == '' || !is_numeric($number) || $number < 0){
            return false;
        }else
        {
            $this->realNumbers[] = $number;
            $_SESSION["realNumbers"] = $this->realNumbers;
            for ($i = 0; $i < $this->telepathCount; $i++) {
                $telepath = $this->telepaths[$i];
                $telepath->calcRate($number);
            }
            return true;
        }
    }
    function getNumberHistory()
    {
        return $this->realNumbers;
    }
    function getTelepaths()
    {
        return $this->telepaths;
    }
}